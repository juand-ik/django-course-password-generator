from django.http.response import HttpResponse
from django.shortcuts import render
from django.http import HttpRequest
import random

# Create your views here.

def home(request):
    return render(request, 'generator/home.html')

def eggs(request):
    return HttpResponse('<h1>Eggs are so tasty</h1>')

def password(request):
    characters = list('abcdefghijklmnopqrstuvwxyz')
    if request.GET.get('uppercase'):
        characters.extend(list('ABCDEFGHIJKLMNOPQRSTUVWXYZ'))
    if request.GET.get('special'):
        characters.extend(list('!@#$%^&*()'))
    if request.GET.get('numbers'):
        characters.extend(list('0123456789'))

    length = int(request.GET.get('length'))
    thepass = ''

    for _ in range(length):
        thepass += random.choice(characters)

    return render(request, 'generator/password.html', {'password': thepass })